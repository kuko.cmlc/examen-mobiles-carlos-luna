package com.ucb.examen_carlosluna.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName="book_table")
class BookModel(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val isbn: Int,
    val title: String,
    val author: String,
    val datePublish: String,
    val pagesNumbers: String,
    val description: String,
    val imageUrl:String
) : Parcelable
package com.ucb.examen_carlosluna.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.ucb.examen_carlosluna.model.BookModel


@Dao
interface IBookDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addBook(book: BookModel)

    @Update
    suspend fun updateBook(book: BookModel)

    @Query("SELECT * FROM book_table ORDER BY id ASC")
    fun readAllData(): LiveData<List<BookModel>>

    @Delete
    fun deleteBook(book: BookModel)
}
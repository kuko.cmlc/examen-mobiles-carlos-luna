package com.ucb.examen_carlosluna.fragments.delete

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.ucb.examen_carlosluna.R
import com.ucb.examen_carlosluna.model.BookModel
import com.ucb.examen_carlosluna.viewModel.BookViewModel
import kotlinx.android.synthetic.main.fragment_book_delete.view.*

class DeleteBookFragment : Fragment() {
    private val args by navArgs<DeleteBookFragmentArgs>()
    private lateinit var mBookViewModel: BookViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_book_delete, container, false)
        mBookViewModel = ViewModelProvider(this).get(BookViewModel::class.java)
        view.txt_book_delete.setText(args.currentBook.title)
        view.btn_approve.setOnClickListener {
            delete()
        }
        view.btn_reject.setOnClickListener {
            goToListBooks()
        }
        return view
    }

    private fun delete() {
        var deleteBook = BookModel(
            args.currentBook.id,
            args.currentBook.isbn,
            args.currentBook.title,
            args.currentBook.author,
            args.currentBook.datePublish,
            args.currentBook.pagesNumbers,
            args.currentBook.description,
            args.currentBook.imageUrl
        )
        mBookViewModel.deleteBook(deleteBook)
        Toast.makeText(requireContext(), "Libro removido", Toast.LENGTH_SHORT).show()
        findNavController().navigate(R.id.action_delete_approved_to_listFragment)

    }

    private fun goToListBooks() {
        findNavController().navigate(R.id.action_delete_reject_to_listFragment)
    }
}
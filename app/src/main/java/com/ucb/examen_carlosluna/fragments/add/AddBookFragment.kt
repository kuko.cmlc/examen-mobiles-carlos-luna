package com.ucb.examen_carlosluna.fragments.add

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.ucb.examen_carlosluna.R
import com.ucb.examen_carlosluna.model.BookModel
import com.ucb.examen_carlosluna.viewModel.BookViewModel
import kotlinx.android.synthetic.main.fragment_book_add.*
import kotlinx.android.synthetic.main.fragment_book_add.view.*

class AddBookFragment : Fragment() {
    private lateinit var mBookViewModel: BookViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_book_add, container, false)
        mBookViewModel = ViewModelProvider(this).get(BookViewModel::class.java)
        view.btn_register_book.setOnClickListener {
            insertDataToDataBase()
        }
        return view
    }

    private fun insertDataToDataBase() {
        val isBN = add_txt_isbn.text
        val title = add_txt_tile.text.toString()
        val pagesNumber = add_txt_pagesNumbers.text.toString()
        val author = add_txt_author.text.toString()
        val datePublish = add_txt_datePublish.text.toString()
        val description = add_txt_description.text.toString()
        val imageUrl = add_txt_imageURL.text.toString()
        if (validateNotEmptyData(title, isBN.toString(), pagesNumber, author, datePublish, description, imageUrl)) {
            val book = BookModel(
                0,
                Integer.parseInt(isBN.toString()) ,
                title,
                author,
                datePublish,
                pagesNumber,
                description,
                imageUrl
            )
            mBookViewModel.addBook(book)
            Toast.makeText(requireContext(), "Libro Registrado", Toast.LENGTH_LONG).show()
            findNavController().navigate(R.id.action_addFragment_to_listFragment)
        } else {
            Toast.makeText(requireContext(), "Los campos no contienen datos.", Toast.LENGTH_LONG).show()
        }
    }

    private fun validateNotEmptyData(
        title: String,
        isBN: String,
        pages: String,
        author: String,
        datePublish: String,
        description: String,
        imageUrl: String
    ): Boolean {
        return !(title.isEmpty() &&
                isBN.isEmpty() &&
                pages.isEmpty() &&
                datePublish.isEmpty() &&
                author.isEmpty() &&
                description.isEmpty() &&
                imageUrl.isEmpty())
    }
}
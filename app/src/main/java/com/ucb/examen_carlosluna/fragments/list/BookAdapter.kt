package com.ucb.examen_carlosluna.fragments.list

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.ucb.examen_carlosluna.R
import com.ucb.examen_carlosluna.model.BookModel
import kotlinx.android.synthetic.main.book_row.view.*

class BookAdapter  : RecyclerView.Adapter<BookAdapter.MyViewHolder>() {

    private var bookList = emptyList<BookModel>()

    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){ }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.book_row, parent, false))
    }

    override fun getItemCount(): Int {
        return  bookList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val picasso = Picasso.get()
        val currentItem =  bookList[position]
        holder.itemView.txt_title.text = currentItem.title
        holder.itemView.txt_author.text = currentItem.author
        holder.itemView.txt_datePublish.text = currentItem.datePublish
        holder.itemView.txt_isbn.text = currentItem.isbn.toString()
        holder.itemView.txt_description.text = currentItem.description
        holder.itemView.txt_pagesNumbers.text = currentItem.pagesNumbers
        picasso.load(currentItem.imageUrl)
            .into(holder.itemView.image_url)


        holder.itemView.btn_edit.setOnClickListener {
            val action = ListBooksFragmentDirections.actionListFragmentToUpdateFragment(currentItem)
            holder.itemView.findNavController().navigate(action)
        }

        holder.itemView.btn_delete.setOnClickListener {
            val action = ListBooksFragmentDirections.actionListFragmentToDeleteFragment(currentItem)
            holder.itemView.findNavController().navigate(action)
        }
    }

    fun setData (books : List <BookModel>){
        this.bookList = books
        notifyDataSetChanged()
    }
}
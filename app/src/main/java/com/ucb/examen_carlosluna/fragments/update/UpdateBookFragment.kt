package com.ucb.examen_carlosluna.fragments.update

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.ucb.examen_carlosluna.R
import com.ucb.examen_carlosluna.model.BookModel
import com.ucb.examen_carlosluna.viewModel.BookViewModel
import kotlinx.android.synthetic.main.book_row.view.*
import kotlinx.android.synthetic.main.fragment_book_add.*
import kotlinx.android.synthetic.main.fragment_book_add.add_txt_isbn
import kotlinx.android.synthetic.main.fragment_book_update.*
import kotlinx.android.synthetic.main.fragment_book_update.view.*

class UpdateBookFragment: Fragment() {

    private val args by navArgs<UpdateBookFragmentArgs>()
    private lateinit var mBookViewModel :BookViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view =  inflater.inflate(R.layout.fragment_book_update, container, false)

        mBookViewModel = ViewModelProvider(this).get(BookViewModel::class.java)

        view.edit_txt_tile.setText(args.currentBook.title)
        view.edit_txt_author.setText(args.currentBook.author)
        view.edit_txt_datePublish.setText(args.currentBook.datePublish)
        view.edit_txt_isbn.setText(args.currentBook.isbn.toString())
        view.edit_txt_description.setText(args.currentBook.description)
        view.edit_txt_pagesNumbers.setText(args.currentBook.pagesNumbers)
        view.edit_txt_imageURL.setText(args.currentBook.imageUrl)

        view.btn_update_book.setOnClickListener {
            updateItem()
        }

        return  view
    }

    private  fun updateItem(){
        val isBN = edit_txt_isbn.text
        val title = edit_txt_tile.text.toString()
        val pagesNumber = edit_txt_pagesNumbers.text.toString()
        val author = edit_txt_author.text.toString()
        val datePublish = edit_txt_datePublish.text.toString()
        val description = edit_txt_description.text.toString()
        val imageUrl = edit_txt_imageURL.text.toString()

        if (validateNotEmptyData(title, isBN.toString(), pagesNumber, author, datePublish, description, imageUrl)) {
            // Create Book Object
            val updateBook = BookModel (
                args.currentBook.id,
                Integer.parseInt(isBN.toString()) ,
                title,
                author,
                datePublish,
                pagesNumber,
                description,
                imageUrl)
            //Update Book Data
            mBookViewModel.updateBook(updateBook)
            Toast.makeText(requireContext(), "Libro actualizado", Toast.LENGTH_SHORT).show()
            // Go to List
            findNavController().navigate(R.id.action_updateFragment_to_listFragment)
        }else{
            Toast.makeText(requireContext(), "Campos no validos", Toast.LENGTH_SHORT).show()
        }
    }

    private fun validateNotEmptyData(
        title: String,
        isBN: String,
        pages: String,
        author: String,
        datePublish: String,
        description: String,
        imageUrl: String
    ): Boolean {
        return !(title.isEmpty() &&
                isBN.isEmpty() &&
                pages.isEmpty() &&
                datePublish.isEmpty() &&
                author.isEmpty() &&
                description.isEmpty() &&
                imageUrl.isEmpty())
    }
}
package com.ucb.examen_carlosluna.repository

import androidx.lifecycle.LiveData
import com.ucb.examen_carlosluna.data.IBookDao
import com.ucb.examen_carlosluna.model.BookModel

class BookRepository (private val bookDao: IBookDao) {
    val readAllData: LiveData<List<BookModel>> = bookDao.readAllData()
    suspend fun  addBook  (book: BookModel){
        bookDao.addBook(book)
    }

    suspend fun updateBook(book: BookModel){
        bookDao.updateBook(book)
    }

    fun deleteBook(book: BookModel){
        bookDao.deleteBook(book)
    }
}
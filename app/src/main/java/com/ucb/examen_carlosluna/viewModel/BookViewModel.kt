package com.ucb.examen_carlosluna.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.ucb.examen_carlosluna.data.BookDatabase
import com.ucb.examen_carlosluna.model.BookModel
import com.ucb.examen_carlosluna.repository.BookRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BookViewModel(application: Application): AndroidViewModel(application) {

    val allBooks: LiveData<List<BookModel>>
    private val repository: BookRepository
    init{
        val bookDao= BookDatabase.getDatabase(application).bookDao()
        repository = BookRepository(bookDao)
        allBooks = repository.readAllData
    }

    fun addBook (book: BookModel){
        viewModelScope.launch(Dispatchers.IO){
            repository.addBook(book)
        }
    }

    fun updateBook (book: BookModel){
        viewModelScope.launch (Dispatchers.IO){
            repository.updateBook(book)
        }
    }

    fun deleteBook (book: BookModel){
        viewModelScope.launch (Dispatchers.IO){
            repository.deleteBook(book)
        }
    }
}